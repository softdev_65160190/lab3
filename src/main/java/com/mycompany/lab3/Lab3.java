/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab3;

/**
 *
 * @author informatics
 */
public class Lab3 {
    
    static boolean checkRow(char[][] table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer && table[i][1] == currentPlayer & table[i][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    static boolean checkCol(char[][] table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == currentPlayer && table[1][i] == currentPlayer && table[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    static boolean checkDiag1(char[][] table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDiag2(char[][] table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][table.length - 1 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }


    static boolean checkDraw(char[][] table, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;
    }

    
    
    static boolean checkWin(char[][] table, char currentPlayer) {
        if(checkRow(table,currentPlayer)||checkCol(table,currentPlayer)||checkDiag1(table,currentPlayer)
        || checkDiag2(table, currentPlayer)|| checkDraw(table, currentPlayer)){
            return true;
        }
        return false;
    }
    

    
    
}
