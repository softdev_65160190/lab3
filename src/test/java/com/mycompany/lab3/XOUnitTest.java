package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class XOUnitTest {

    public XOUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_Horizon1_output_true() {
        char[][] table = {{'O', 'O', 'O'},
        {'4', '5', '6'},
        {'1', '2', '3'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);

    }

    @Test
    public void testCheckWin_O_Horizon2_output_true() {
        char[][] table = {{'7', '8', '9'},
        {'O', 'O', 'O'},
        {'1', '2', '3'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Horizon3_output_true() {
        char[][] table = {{'7', '8', '9'},
        {'4', '5', '6'},
        {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Horizon1_output_true() {
        char[][] table = {{'X', 'X', 'X'},
        {'4', '5', '6'},
        {'1', '2', '3'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Horizon2_output_true() {
        char[][] table = {{'7', '8', '9'},
        {'X', 'X', 'X'},
        {'1', '2', '3'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Horizon3_output_true() {
        char[][] table = {{'7', '8', '9'},
        {'4', '5', '6'},
        {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Vertical1_output_true() {
        char[][] table = {{'O', '8', '9'},
        {'O', '5', '6'},
        {'O', '2', '3'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Vertical2_output_true() {
        char[][] table = {{'7', 'O', '9'},
        {'4', 'O', '6'},
        {'1', 'O', '3'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Vertical3_output_true() {
        char[][] table = {{'7', '8', 'O'},
        {'4', '5', 'O'},
        {'1', '2', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Vertical1_output_true() {
        char[][] table = {{'X', '8', '9'},
        {'X', '5', '6'},
        {'X', '2', '3'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Vertical2_output_true() {
        char[][] table = {{'7', 'X', '9'},
        {'4', 'X', '6'},
        {'1', 'X', '3'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Vertical3_output_true() {
        char[][] table = {{'7', '8', 'X'},
        {'4', '5', 'X'},
        {'1', '2', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Diagnal1_Out_put_true() {
        char[][] table = {{'O', '8', '9'},
        {'4', 'O', '6'},
        {'1', '2', 'O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_O_Diagnal2_Out_put_true() {
        char[][] table = {{'7', '8', 'O'},
        {'4', 'O', '6'},
        {'O', '2', '3'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Diagnal1_Out_put_true() {
        char[][] table = {{'X', '8', '9'},
        {'4', 'X', '6'},
        {'1', '2', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckWin_X_Diagnal2_Out_put_true() {
        char[][] table = {{'7', '8', 'X'},
        {'4', 'X', '6'},
        {'X', '2', '3'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }

    @Test
    public void testCheckDraw_output_true() {
        char[][] table = {{'O', 'X', 'O'},
                            {'X', 'O', 'X'},
                            {'X', 'O', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckDraw_output_false() {
        char[][] table = {{'O', 'X', 'O'},
                            {'X', 'O', 'X'},
                            {'1', 'O', 'X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(false, result);
    }

}
